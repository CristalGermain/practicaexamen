package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

public class ProductsExecution {
    private Context context;
    private DbHelper dbHelper;
    private SQLiteDatabase db;

    private String [] columnToRead = new String[] {
            DefinirTabla.Producto._ID,
            DefinirTabla.Producto.MARCA,
            DefinirTabla.Producto.PERECEDERO,
            DefinirTabla.Producto.PRECIO,
            DefinirTabla.Producto.CODIGO,
            DefinirTabla.Producto.NOMBRE

    };

    public ProductsExecution(Context context) {
        this.context = context;
        dbHelper = new DbHelper(this.context);
    }

    public void openDataBase() { db = dbHelper.getWritableDatabase(); }

    public long insertarProducto(Producto p) {
        ContentValues values = new ContentValues();

        values.put(DefinirTabla.Producto.MARCA, p.getMarca());
        values.put(DefinirTabla.Producto.PERECEDERO, p.getPerecedero());
        values.put(DefinirTabla.Producto.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Producto.CODIGO, p.getCodigo());
        values.put(DefinirTabla.Producto.NOMBRE, p.getNombre());

        return  db.insert(DefinirTabla.Producto.TABLA_NAME, null, values);
    }

    public long updateProducto(Producto p, int id) {
        ContentValues values = new ContentValues();

        values.put(DefinirTabla.Producto.MARCA, p.getMarca());
        values.put(DefinirTabla.Producto.PERECEDERO, p.getPerecedero());
        values.put(DefinirTabla.Producto.PRECIO, p.getPrecio());
        values.put(DefinirTabla.Producto.CODIGO, p.getCodigo());
        values.put(DefinirTabla.Producto.NOMBRE, p.getNombre());

        String criterio = DefinirTabla.Producto.CODIGO + " = " + id;

        return db.update(DefinirTabla.Producto.TABLA_NAME, values, criterio, null);
    }

    public int deleteProducto(int id) {
        String criterio = DefinirTabla.Producto.CODIGO + " = " + id;
        return db.delete(DefinirTabla.Producto.TABLA_NAME, criterio, null);
    }

    public Producto readProducto(Cursor cursor) {
        Producto p = new Producto();
        try {
            p.set_ID(cursor.getLong(0));
            p.setNombre(cursor.getString(5));
            p.setMarca(cursor.getString(1));
            p.setPrecio(cursor.getFloat(3));
            p.setPerecedero(cursor.getInt(2));
            p.setCodigo(cursor.getInt(4));
        } catch (Exception e){
            p=null;
        }
        return p;
    }

    public Producto getProducto(int id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.Producto.TABLA_NAME,
                columnToRead,
                DefinirTabla.Producto.CODIGO + " = ?",
                new String[] {String.valueOf(id)}, null, null, null );
        cursor.moveToFirst();
        Producto producto = readProducto(cursor);
        cursor.close();
        return producto;
    }

    public ArrayList<Producto> allProducto() {
        ArrayList<Producto> contactos = new ArrayList<Producto>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.query(DefinirTabla.Producto.TABLA_NAME,
                null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Producto p = readProducto(cursor);
            cursor.moveToNext();
            contactos.add(p);
        }

        cursor.close();
        return contactos;
    }

    public void cerrarDataBase() {
        dbHelper.close();
    }


}
