package com.example.practicaexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.database.SQLException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import database.Producto;
import database.ProductsExecution;

public class ProductoActivity extends AppCompatActivity {

    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioButton radioP;
    private RadioButton radioNP;
    private Button btnActualizar;
    private Button btnBuscar;
    private Button btnBorrar;
    private Button btnCerrar;
    private Button btnLimpiar;

    private ProductsExecution db;
    private Producto producto;
    private Producto savedProduct;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);

        txtCodigo = findViewById(R.id.txtCodigo);
        btnBuscar = findViewById(R.id.btnBuscar);
        txtNombre = findViewById(R.id.txtNombre);
        txtMarca = findViewById(R.id.txtMarca);
        txtPrecio = findViewById(R.id.txtPrecio);
        radioP = findViewById(R.id.radioPP);
        radioNP = findViewById(R.id.radioNPP);
        btnBorrar = findViewById(R.id.btnBorrar);
        btnActualizar = findViewById(R.id.btnActualizar);
        btnCerrar = findViewById(R.id.btnCerrar);
        btnLimpiar = findViewById(R.id.btnLimpiar);

        db = new ProductsExecution(this);

        btnBorrar.setEnabled(false);
        btnActualizar.setEnabled(false);

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtCodigo.getText().toString().matches("")) {
                    Toast.makeText(ProductoActivity.this, "Inserte un código", Toast.LENGTH_SHORT).show();
                } else {
                    try {
                        db.openDataBase();
                        producto = db.getProducto(Integer.parseInt(txtCodigo.getText().toString()));
                        db.cerrarDataBase();
                        savedProduct = producto;
                        if (producto != null) {
                            btnBorrar.setEnabled(true);
                            btnActualizar.setEnabled(true);
                            txtNombre.setText(producto.getNombre());
                            txtMarca.setText(producto.getMarca());
                            txtPrecio.setText(String.valueOf(producto.getPrecio()));
                            if (producto.getPerecedero() > 0) {
                                radioP.setChecked(true);
                            } else
                                radioNP.setChecked(true);
                        } else {
                            Toast.makeText(ProductoActivity.this, "No se encontraron resultados", Toast.LENGTH_SHORT).show();
                            limpiar();
                        }
                    }
                    catch (Exception e)
                    {
                        Toast.makeText(ProductoActivity.this, "No se encontraron resultados", Toast.LENGTH_SHORT).show();
                        limpiar();
                    }

                }
            }
        });

        btnActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNombre.getText().toString().matches("")  || txtMarca.getText().toString().matches("") ||
                        txtPrecio.getText().toString().matches("") || txtCodigo.getText().toString().matches("")) {
                    Toast.makeText(ProductoActivity.this, "Llene correctamente todos los campos",Toast.LENGTH_SHORT).show();
                } else {
                    Producto product = new Producto();
                    product.setCodigo(savedProduct.getCodigo());
                    product.setNombre(txtNombre.getText().toString());
                    product.setMarca(txtMarca.getText().toString());
                    product.setPrecio(Float.parseFloat(txtPrecio.getText().toString()));
                    product.setPerecedero(radioP.isChecked() ? 1 : 0);

                    db.openDataBase();
                    if(db.updateProducto(product,product.getCodigo()) > -1) {
                        Toast.makeText(ProductoActivity.this, "Se actualizó con éxito", Toast.LENGTH_SHORT).show();
                        btnBorrar.setEnabled(false);
                        btnActualizar.setEnabled(false);
                    } else {
                        Toast.makeText(ProductoActivity.this, "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
                    }
                    db.cerrarDataBase();
                }
                limpiar();
            }
        });

        btnBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.openDataBase();

                db.deleteProducto(Integer.parseInt(txtCodigo.getText().toString()));
                Toast.makeText(ProductoActivity.this, "Se ha eliminado con éxito el producto con ID  " + Long.parseLong(txtCodigo.getText().toString()), Toast.LENGTH_SHORT).show();
                db.cerrarDataBase();
                btnBorrar.setEnabled(false);
                btnActualizar.setEnabled(false);
                limpiar();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
                btnBorrar.setEnabled(false);
                btnActualizar.setEnabled(false);
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void limpiar(){
        txtPrecio.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        txtCodigo.setText("");
        radioP.setChecked(true);

    }
}
