package com.example.practicaexamen;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import database.Producto;
import database.ProductsExecution;

public class MainActivity extends AppCompatActivity {

    private EditText txtCodigo;
    private EditText txtNombre;
    private EditText txtMarca;
    private EditText txtPrecio;
    private RadioButton radioP;
    private RadioButton radioNP;
    private Button btnGuardar;
    private Button btnLimpiar;
    private Button btnEditar;
    private Button btnCerrar;

    private ProductsExecution db;
    private Producto savedProduct;
    private long id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtCodigo = (EditText) findViewById(R.id.txtCodigo);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        txtMarca = (EditText) findViewById(R.id.txtMarca);
        txtPrecio = (EditText) findViewById(R.id.txtPrecio);
        radioP = (RadioButton) findViewById(R.id.radioP);
        radioNP = (RadioButton) findViewById(R.id.radioNP);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnEditar = (Button) findViewById(R.id.btnEditar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);

        db = new ProductsExecution(MainActivity.this);


        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtNombre.getText().toString().matches("")  ||
                        txtMarca.getText().toString().matches("") ||
                        txtPrecio.getText().toString().matches("") ||
                        txtCodigo.getText().toString().matches("")) {
                    Toast.makeText(MainActivity.this, "Llene correctamente todos los campos",Toast.LENGTH_SHORT).show();
                } else {
                    Producto p= new Producto(Integer.parseInt(txtCodigo.getText().toString()),
                                            txtNombre.getText().toString(),
                                            txtMarca.getText().toString(),
                                            (radioP.isChecked() ? 1:0),
                                            Long.parseLong(txtPrecio.getText().toString()));
                    db.openDataBase();
                    try {
                        Producto temporal = db.getProducto(Integer.parseInt(txtCodigo.getText().toString()));
                        if (temporal == null) {

                            if (savedProduct != null) {
                                long id = db.updateProducto(p, savedProduct.getCodigo());
                                Toast.makeText(MainActivity.this, "El producto se actualizó con éxito: " + id, Toast.LENGTH_SHORT).show();
                                savedProduct = null;
                            } else {
                                long id = db.insertarProducto(p);
                                if (id > -1)
                                    Toast.makeText(MainActivity.this, "El producto se guardó con éxito: " + id, Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(MainActivity.this, "Ha ocurrido un error", Toast.LENGTH_SHORT).show();
                            }
                            db.cerrarDataBase();
                            limpiar();
                        } else {
                            Toast.makeText(MainActivity.this, "Este código ya existe", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "El código ya existe error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });


        btnEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ProductoActivity.class);
                startActivityForResult(i,0);
            }
        });


        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void limpiar(){
        txtPrecio.setText("");
        txtNombre.setText("");
        txtMarca.setText("");
        txtCodigo.setText("");
        radioP.setChecked(true);

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (Activity.RESULT_OK == resultCode) {
            Producto p = (Producto) intent.getSerializableExtra("contacto");
            savedProduct = p;
            id = p.get_ID();
            txtNombre.setText(p.getNombre());
            txtMarca.setText(p.getMarca());
            txtPrecio.setText( String.valueOf(p.getPrecio()));

            if(p.getPerecedero() > 0 )
            {
                radioP.setChecked(true);
                radioNP.setChecked(false);
            } else {
                radioP.setChecked(false);
                radioNP.setChecked(true);
            }
        } else {
            limpiar();
        }


    }
}
